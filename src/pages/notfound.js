import React from "react";
import Layout from "../components/layout";

const NotFound = () => {
    return (
        <Layout>
            <h1>The page you are looking for does not exist.<br>Please check your path.</br></h1>
        </Layout>
    )
}

export default NotFound