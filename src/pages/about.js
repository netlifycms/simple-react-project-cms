import React from "react";
import Layout from "../components/layout";

const About = () => {
    return (
        <Layout>
            <h1>This is the about page</h1>
            <div className="page-content">
                <p>This is the page content</p>    
            </div>
        </Layout>
    )
}

export default About