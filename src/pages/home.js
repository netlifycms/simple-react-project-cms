import React from "react";
import Layout from "../components/layout";

const Home = () => {
    return (
        <div>
            <Layout>
                <p>This is the core of the Layout component</p>
            </Layout>
        </div>
    )
}

export default Home