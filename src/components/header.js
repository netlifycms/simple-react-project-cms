import React from 'react'

const Header = () => {
  return (
    <div className="header">
        <h1>React Markdown Blog</h1>
    </div>
  )
}

export default Header