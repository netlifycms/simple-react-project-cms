import React from 'react'

const Footer = () => {
  return (
    <div className="footer">
        <hr/>
        <p>&copy; Trololo zeubi {new Date().getFullYear()}</p>
    </div>
)
}

export default Footer